from django.urls import path

from . import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [ 
    # Authentification
    path('', views.secure, name='secure'),
    path('login/', views.login, name='login'),
    path('lostpassword/', views.lostpassword, name='lostpassword'),  
    path('forgetpassword/', views.forgetpassword, name='forgetpassword'), 
    path('register/', views.register, name='register'),  
    path('logout/', views.logout, name='logout'),

    # Accueil
    path('home/', views.home, name='home'),

    # Twouitts
    path('twouitt/add', views.add_twouitt, name='addtwouitt'),
    path('twouitt/<slug:slug>', views.twouitt, name='twouitt'),
    path('retwouitts/<slug:slug>', views.retwouitt, name='retwouitt'),
    path('twouitt/<slug:slug>/favoris/add', views.add_favoris, name='add_favoris'),
    path('twouitt/<slug:slug>/favoris/delete', views.delete_favoris, name='delete_favoris'),

    # Profil
    path('profil/<slug:slug>', views.profil, name='profil'),
    path('settings/general', views.settings_general, name='settings_general'),
    path('settings/profil', views.settings_profil_info, name='settings_profil_info'),
    path('settings/account', views.settings_account_info, name='settings_account_info'),
    path('settings/password', views.settings_password, name='settings_password'),

    path('settings/general/edit', views.settings_general_edit, name='settings_general_edit'),
    path('settings/profil/avatar/edit', views.settings_profil_avatar_edit, name='settings_profil_avatar_edit'),
    path('settings/profil/cover/edit', views.settings_profil_cover_edit, name='settings_profil_cover_edit'),
    path('settings/profil/edit', views.settings_profil_info_edit, name='settings_profil_info_edit'),
    path('settings/account/edit', views.settings_account_info_edit, name='settings_account_info_edit'),
    path('settings/password/edit', views.settings_password_edit, name='settings_password_edit'),

    
    path('notifications', views.notifications, name='notifications'),

    # Messages
    path('messages', views.messages, name='messages'),
    path('messages/<slug:slug>', views.get_messages_conversation, name='messages_conversation'),
    path('messages/send/<slug:slug>', views.send_message, name='send_message'),
    path('messages/conversations', views.get_conversations, name='list_conversations'),
    path('messages/read/all', views.mark_all_messages_read, name='mark_all_messages_read'),
    path('abonnement/add/<slug:slug>', views.create_abonnement, name='add_abonnement'),
    path('abonnement/delete/<slug:slug>', views.delete_request, name='delete_request'),

    # Friends
    path('friends/list', views.friends_list, name='friends_list'),
    path('friends/request', views.friends_request, name='friends_request'),
    path('deleteuser/', views.deleteuser, name='deleteuser'),

    path('search/', views.search, name='search'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)