$(document).ready(function(){
    // Renvoie le chemin de la page actuelle
    var pathname = window.location.pathname;

    if (pathname == "/messages")
    {
        // Permet d'afficher une conversation
        $(".chat-widget-message.conversation").click(function() {
            $(".chat-widget-message.conversation").removeClass("active")
            $(this).addClass("active")

            let infos_user = {
                "id" : $(this).attr('id').split('-')[1],
                "name" : $(this).find(".conversation-name").text().trim(),
                "photo" : $(this).find(".conversation-photo").attr('data-src'),
                "status" : $(this).find(".conversation-status").val(),
            }

            $.ajax({
                // Ajour du csrf token avant l'envoi de la requête ajax
                beforeSend: function(xhr, settings) {
                     if (!/^https?:.*/.test(settings.url)  && settings.type == "POST") {
                         // attachement du token dans le header
                         xhr.setRequestHeader("X-CSRFToken",  Cookies.get('csrftoken'));
                     }
                },
                url: '/messages/'+infos_user.id,
                method: 'POST',
                dataType: 'json',
                success: function (data) {
                    view_conversation(infos_user, data)
                }
              })


            $(".simplebar-content-wrapper").scrollTop($(".simplebar-content-wrapper").height());

        });

        // Fonction

        // Permet de générer l'affichage d'une conversation
        function view_conversation(infos_user, data) {
        let html = "<div class=\"chat-widget\" id=\"conversation-current\">"

        // Header
        html += "<div class=\"chat-widget-header\" id=\"conversation-"+ infos_user.id +"\">"
        html += "<div class=\"chat-widget-settings\">"
        html += "<div class=\"post-settings-wrap\">"
        html += "<div class=\"post-settings widget-box-post-settings-dropdown-trigger\">"
        html += "<svg class=\"post-settings-icon icon-more-dots\"><use xlink:href=\"#svg-more-dots\"></use></svg>"
        html += "</div>"
        html += "<div class=\"simple-dropdown widget-box-post-settings-dropdown\">"
        html += "<p class=\"simple-dropdown-link\">Signaler</p>"
        html += "<p class=\"simple-dropdown-link\">Bloquer</p>"
        html += "</div>"
        html += "</div>"
        html += "</div>"

        html += "<div class=\"user-status\">"
        html += "<div class=\"user-status-avatar\">"
        html += "<div class=\"user-avatar small no-outline\">"
        html += "<div class=\"user-avatar-content\">"
        html += "<div class=\"hexagon-image-30-32\" data-src=\"" + infos_user.photo + "\"></div>"
        html += "</div>"
        html += "<div class=\"user-avatar-progress\">"
        html += "<div class=\"hexagon-progress-40-44\"></div>"
        html += "</div>"
        html += "<div class=\"user-avatar-progress-border\">"
        html += "<div class=\"hexagon-border-40-44\"></div>"
        html += "</div>"
        html += "</div>"
        html += "</div>"
        html += "<p class=\"user-status-title\"><span class=\"bold\">"+ infos_user.name +"</span></p>"
        html += "</div>"
        html += "</div>"

        // Conversation
        html += "<div class=\"chat-widget-conversation\" data-simplebar>"

        $.each(data, function(i, item) {
            if (JSON.parse(document.getElementById('profil_id').textContent) == item.expediteur_id)
            {
                // Partie Message Utilisateur connecté
                html += "<div class=\"chat-widget-speaker right\">"
                html += "<p class=\"chat-widget-speaker-message\">"+ item.message +"</p>"
                html += "<p class=\"chat-widget-speaker-timestamp\">"+ item.created_at +"</p>"
                html += "</div>"
            }
            else
            {
                // Partie Message Ami
                html += "<div class=\"chat-widget-speaker left\">"
                html += "<div class=\"chat-widget-speaker-avatar\">"
                html += "<div class=\"user-avatar tiny no-border\">"
                html += "<div class=\"user-avatar-content\">"
                html += "<div class=\"hexagon-image-24-26\" data-src=\"" + infos_user.photo + "\"></div>"
                html += "</div>"
                html += "</div>"
                html += "</div>"
                html += "<p class=\"chat-widget-speaker-message\">"+ item.message +"</p>"
                html += "<p class=\"chat-widget-speaker-timestamp\">"+ item.created_at +"</p>"
                html += "</div>"
            }
        });

        html += "</div>"


        // Form
        html += "<form class=\"chat-widget-form\" id=\"form-messages\">"
        // html += "<input type=\"hidden\" name=\"csrfmiddlewaretoken\" value=\""+ Cookies.get('csrftoken') +"\">"
        html += "<div class=\"form-row split\">"
        html += "<div class=\"form-item\">"
        html += "<div class=\"interactive-input small\">"
        html += "<input type=\"text\" id=\"chat-widget-message-text-2\" name=\"message\" placeholder=\"Ecrire un message...\" required>"
        html += "<div class=\"interactive-input-action\">"
        html += "<svg class=\"interactive-input-action-icon icon-cross-thin\"><use xlink:href=\"#svg-cross-thin\"></use></svg>"
        html += "</div>"
        html += "</div>"
        html += "</div>"
        html += "<div class=\"form-item auto-width\">"
        html += "<p class=\"button primary padded\" id=\"button-send-message\">"
        html += "<svg class=\"button-icon no-space icon-send-message\"><use xlink:href=\"#svg-send-message\"></use></svg>"
        html += "</p>"
        html += "</div>"
        html += "</div>"
        html += "</form>"

        html += "</div>"

        $('#conversation-current').remove();
        $('#messages-view').append(html);

        $("#button-send-message").click(function() {

            let message = $('#form-messages').find("input").val()

            if (message.length > 0)
            {
                $.ajax({
                    // Ajour du csrf token avant l'envoi de la requête ajax
                    beforeSend: function(xhr, settings) {
                         if (!/^https?:.*/.test(settings.url)  && settings.type == "POST") {
                             // attachement du token dans le header
                             xhr.setRequestHeader("X-CSRFToken",  Cookies.get('csrftoken'));
                         }
                    },
                    data: { "message": message},
                    url: '/messages/send/'+infos_user.id,
                    method: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        view_conversation(infos_user, data)
                    }
                  });
            }
        });
        $.getScript("/static/js/app.bundle.min.js")
    }
    }

    // Marquer tous les messages comme lus
    $("#mark_read_all_messages").click(function() {
            $.ajax({
                // Ajour du csrf token avant l'envoi de la requête ajax
                beforeSend: function(xhr, settings) {
                     if (!/^https?:.*/.test(settings.url)  && settings.type == "POST") {
                         // attachement du token dans le header
                         xhr.setRequestHeader("X-CSRFToken",  Cookies.get('csrftoken'));
                     }
                },
                url: '/messages/read/all'
            })
        })


    $('.dropdown-box-list-item').click(function() {
        let id = $(this).attr('id')
        window.location.href = "/messages";
        $('#conversation-'+id).click()
    })
})