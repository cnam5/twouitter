$(document).ready(function(){

    // Renvoie le chemin de la page actuelle
    var pathname = window.location.pathname;

    // 1 : Permet de sélectionner l'élément dans le menu qui correspond à la page où on se situe actuellement
    // 2 : Permet de définir l'action du bouton "enregister" dans le menu qui correspond à la page où on se situe actuellement
    switch(pathname) {
        case "/settings/profil":
            $("#accordion-profil").addClass("accordion-open")
            $("#accordion-profil").parent().css('height', '161px')
            $("#accordion-profil #profil-info").addClass("active")

            $("#save-change").click(function() {
                $("#form-profil-info").submit()
            });
            break;
        case "/notifications":
            $("#accordion-profil").addClass("accordion-open")
            $("#accordion-profil").parent().css('height', '161px')
            $("#accordion-profil #profil-notifications").addClass("active")
        break;
        case "/messages":
            $("#accordion-profil").addClass("accordion-open")
            $("#accordion-profil").parent().css('height', '161px')
            $("#accordion-profil #profil-messages").addClass("active")
        break;
        case "/friends/request":
            $("#accordion-profil").addClass("accordion-open")
            $("#accordion-profil").parent().css('height', '161px');
            $("#accordion-profil #profil-friends-request").addClass("active")
        break;
        
        case "/settings/account":
            $("#accordion-account").addClass("accordion-open")
            $("#accordion-account").parent().css('height', '127px')
            $("#accordion-account #account-info").addClass("active")

            $("#save-change").click(function() {
                $("#form-account-info").submit()
            });
            break;        
        case "/settings/password":
            $("#accordion-account").addClass("accordion-open")
            $("#accordion-account").parent().css('height', '127px');
            $("#accordion-account #account-password").addClass("active")

            $("#save-change").click(function() {
                $("#form-password").submit()
            });
            break;
        case "/settings/general":
            $("#accordion-account").addClass("accordion-open")
            $("#accordion-account").parent().css('height', '127px');
            $("#accordion-account #account-general").addClass("active")

            $("#save-change").click(function() {
                // Définie les valeurs des inputs lors que l'on change ses préférences de notifications(commentaires, tags, demandes d'ami) par mail
                $("#general-commentaires").parent().hasClass("active") ? $("#general-commentaires").val(1) : $("#general-commentaires").val(0)
                $("#general-tags").parent().hasClass("active") ? $("#general-tags").val(1) : $("#general-tags").val(0)
                $("#general-friends-request").parent().hasClass("active") ? $("#general-friends-request").val(1) : $("#general-friends-request").val(0)

                $("#form-general").submit()
            });
            break;
        default:
          // Nothing to do
    }

    // Replie le menu du compte si on clique sur le header du menu du profil 
    $("#header-profil").click(function() {
        $("#accordion-account").removeClass("accordion-open")
        $("#accordion-account").parent().css('height', '0px');
    });

    // Replie le menu du profil si on clique sur le header du menu du compte 
    $("#header-account").click(function() {
        $("#accordion-profil").removeClass("accordion-open")
        $("#accordion-profil").parent().css('height', '0px');
    });

    // Permet de cliquer sur le inputs files cachées en cliquant sur la div d'uploads avatar
    $("#upload-box-avatar").click(function() {
        $("#id_photo").click();
    });

    // Permet de cliquer sur le inputs files cachées en cliquant sur la div d'uploads banniere
    $("#upload-box-cover").click(function() {
        $("#id_banniere").click();
    });


});