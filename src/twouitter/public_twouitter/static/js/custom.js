/**
 * Recupere le cookie {name}
 * @param name nom du cookie
 * @returns string valeur du cookie
 */
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

/**
 * Preview image
 * @param input formulaire
 */
function upload_img(input) {
  if (input.files && input.files[0]) {
    var src = URL.createObjectURL(input.files[0]);
    $('.preview-image-twouitt').attr('src', src);
    $('.preview-image-twouitt').css('background','rgba(0, 0, 0, 0) url("' + src + '") no-repeat scroll center center / cover');
    $('.quick-post-header').addClass('active');
  }
}

$(document).ready(function(){
   $('.favoris-button').click(function () {
       var action = "";
       var succes;
       var slug = $(".twouitt .hidden-slug").val();
       var csrftoken = getCookie('csrftoken');

       if($(this).hasClass('button-active')){
           succes = () => $(this).removeClass('button-active');
           action = 'delete';
       }else {
           succes = () => $(this).addClass('button-active');
           action = 'add'
       }
       $.ajax({
            url: "/twouitt/" + slug + "/favoris/" + action,
            type: 'post',
            headers: {
                "X-CSRFToken": csrftoken
            },
            dataType: 'json',
            success: succes,
            error: (e) => console.log("Erreur sur un favoris => " + e)
        });
   });

   $("#add-img-twouitt").click(function () {
       $("#id_photo").click();
   })

    $("#quick-post-text").on('keydown', function () {
        var nb_char = $(this).val().length;
        $("#nb-char").text(nb_char);
    })

    $("#submit-quick").click(function (){
        $("#form-create-twouitt").submit();
    })

    $("#cancel-quick").click(function () {
        $("#quick-post-text").val("");
        $('.preview-image-twouitt').attr('src', "");
        $('.preview-image-twouitt').css('background',"");
        $('.quick-post-header').removeClass('active');
        $('#id_photo').val("");
    })
});