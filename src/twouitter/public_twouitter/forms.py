from django import forms
from .models import Profil, Message

class AvatarForm(forms.ModelForm):
    photo = forms.ImageField(label='', widget = forms.FileInput(attrs={'onchange' : 'this.form.submit()', 'accept' : 'image/png, image/jpeg', 'hidden' : 'hidden'}))
    class Meta:
        model = Profil
        fields = {'photo'}

class CoverForm(forms.ModelForm):
    banniere = forms.ImageField(label='', widget = forms.FileInput(attrs={'onchange' : 'this.form.submit()', 'accept' : 'image/png, image/jpeg', 'hidden' : 'hidden'}))
    class Meta:
        model = Profil
        fields = {'banniere'}

class ImageForm(forms.ModelForm):
    photo = forms.ImageField(label='', widget = forms.FileInput(attrs={'onchange' : 'upload_img(this)', 'accept' : 'image/png, image/jpeg', 'hidden' : 'hidden'}))
    class Meta:
        model = Message
        fields = {'photo'}