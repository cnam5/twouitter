import datetime
from django import template

register = template.Library()


@register.simple_tag
def mutual_follow(user, friend):
    is_following_me = False
    try:
        is_following_me = user.abonnes.get(pk=friend.pk).exists()
    except:
        return "probleme"

    if is_following_me:
        return "Vous vous suivez mutuellement"
    return "t"


@register.inclusion_tag('twouitt/twouitt_skeleton.html')
def twouitt_tag(msg):
    return {'msg': msg}


@register.inclusion_tag('twouitt/retwouitt_skeleton.html')
def retwouitt_tag(msg):
    return {'msg': msg}
