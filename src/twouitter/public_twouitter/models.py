from datetime import time
from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone


# Create your models here.

# TODO
# verif les created at etc
# verif media imagefield filefield

class ModelLogged(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Profil(ModelLogged):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )

    birthday = models.DateField()
    city = models.TextField(max_length=100)
    country = models.TextField(max_length=100)
    about_me = models.TextField()
    suspended = models.BooleanField(default=0)

    photo = models.ImageField(upload_to='avatar/', default='avatar/default-avatar.png')
    banniere = models.ImageField(upload_to='cover/', default='cover/default-cover.jpg')

    abonnement = models.ManyToManyField('self', symmetrical=False, related_name="abonnes")
    user_bloques = models.ManyToManyField('self', symmetrical=False, related_name="bloques_par")

    class Gender(models.TextChoices):
        HOMME = 'H', _('Homme')
        FEMME = 'F', _('Femme')

    gender = models.CharField(
        max_length=1,
        choices=Gender.choices,
    )

    def nb_photos(self):
        return self.message_set.filter(photo__isnull=False).count()

    def preview_photos(self, n=12):
        r = self.message_set.exclude(photo__isnull=True)
        return r

    def all_photos(self):
        return self.message_set.filter(photo__isnull=False)

class Notification(ModelLogged):
    user = models.ForeignKey(
        Profil,
        on_delete=models.CASCADE)
    message = models.TextField()
    is_read = models.BooleanField()

class MessagePrive(ModelLogged):
    expediteur = models.ForeignKey(
        Profil,
        related_name= 'expediteur',
        on_delete=models.CASCADE)
    destinataire = models.ForeignKey(
        Profil,
        related_name= 'destinataire',
        on_delete=models.CASCADE)
    message = models.TextField()
    is_read = models.BooleanField()

class Message(ModelLogged):
    user = models.ForeignKey(
        Profil,
        on_delete=models.CASCADE)
    message_parent = models.ForeignKey(
        'self',
        related_name= 'parent',
        null= True,
        blank= True,
        on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='image/')
    message = models.TextField()
    slug = models.TextField(max_length=20)
    
    # Reverse relation
    favoris = models.ManyToManyField(
        Profil,
        related_name='favoris')

    def get_comments(self):
        return Message.objects.filter(message_parent=self)

class Retweet(ModelLogged):
    user = models.ForeignKey(
        Profil,
        on_delete=models.CASCADE)
    message = models.ForeignKey(
        Message,
        on_delete=models.CASCADE)
    citation = models.TextField()

class SignalementMessage(ModelLogged):
    user = models.ForeignKey(
        Profil,
        on_delete=models.CASCADE)
    message = models.ForeignKey(
        Message,
        on_delete=models.CASCADE)
    motif = models.TextField()

class SignalementUser(ModelLogged):
    user = models.ForeignKey(
        Profil,
        on_delete=models.CASCADE)
    message = models.ForeignKey(
        Message,
        on_delete=models.CASCADE)
    motif = models.TextField()

class Preferences(ModelLogged):
    profil = models.OneToOneField(
        Profil,
        on_delete=models.CASCADE
    )

    commentaires = models.BooleanField()
    tags = models.BooleanField()
    friends_request = models.BooleanField()


    class Confidentialite(models.TextChoices):
        PUBLIC = 'Public', _('Public')
        AMIS = 'Amis', _('Amis')
        PRIVEE = 'Privee', _('Privee')

    view_profil = models.CharField(
        max_length=6,
        choices=Confidentialite.choices,
        default=Confidentialite.PUBLIC
    )

    send_request_friends = models.CharField(
        max_length=6,
        choices=Confidentialite.choices,
        default=Confidentialite.PUBLIC
    )