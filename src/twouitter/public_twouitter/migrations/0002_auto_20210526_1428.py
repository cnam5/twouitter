# Generated by Django 3.2 on 2021-05-26 12:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('public_twouitter', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='favoris',
            field=models.ManyToManyField(related_name='favoris', to='public_twouitter.Profil'),
        ),
        migrations.AddField(
            model_name='profil',
            name='follow',
            field=models.ManyToManyField(related_name='_public_twouitter_profil_follow_+', to='public_twouitter.Profil'),
        ),
        migrations.AddField(
            model_name='profil',
            name='user_bloques',
            field=models.ManyToManyField(related_name='_public_twouitter_profil_user_bloques_+', to='public_twouitter.Profil'),
        ),
    ]
