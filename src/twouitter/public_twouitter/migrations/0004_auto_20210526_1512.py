# Generated by Django 3.2 on 2021-05-26 13:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('public_twouitter', '0003_auto_20210526_1510'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='message',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='message',
            name='updated_at',
        ),
        migrations.RemoveField(
            model_name='messageprive',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='messageprive',
            name='updated_at',
        ),
        migrations.RemoveField(
            model_name='notification',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='notification',
            name='updated_at',
        ),
        migrations.RemoveField(
            model_name='profil',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='profil',
            name='updated_at',
        ),
        migrations.RemoveField(
            model_name='retweet',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='retweet',
            name='updated_at',
        ),
        migrations.RemoveField(
            model_name='signalementmessage',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='signalementmessage',
            name='updated_at',
        ),
        migrations.RemoveField(
            model_name='signalementuser',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='signalementuser',
            name='updated_at',
        ),
    ]
