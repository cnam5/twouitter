from django.contrib import admin

from .models import *

@admin.register(Profil)
class ProfilAdmin(admin.ModelAdmin):
    pass

@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    pass

@admin.register(MessagePrive)
class MessagePriveAdmin(admin.ModelAdmin):
    pass

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    pass

@admin.register(Retweet)
class RetweetAdmin(admin.ModelAdmin):
    pass

@admin.register(SignalementMessage)
class SignalementMessageAdmin(admin.ModelAdmin):
    pass

@admin.register(SignalementUser)
class SignalementUserAdmin(admin.ModelAdmin):
    pass

@admin.register(Preferences)
class PreferencesAdmin(admin.ModelAdmin):
    pass


