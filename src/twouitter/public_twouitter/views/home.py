from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from ..forms import ImageForm
from ..models import Message
from django.http.response import Http404

from django.contrib.auth.decorators import login_required

# # # # # # # # # # #
#                   #
#        Home       #
#                   #
# # # # # # # # # # #

@login_required
def home(request):
    template = loader.get_template("home/index.html")
    context = {
        'title': "Twouitter | Accueil",
        'image_form': ImageForm(),
        'timeline': generateTimeline(request.user),
    }
    return HttpResponse(template.render(context, request))

def search(request):
    if request.method == "GET":
        query = request.GET['search_main']
        if query:
            results_user = User.objects.filter(username__contains=query)
            results_msg = Message.objects.filter(message__contains=query)

            template = loader.get_template("home/search.html")
            context = {
                'title': "Twouitter | Accueil",
                'results_user': results_user,
                'results_msg': results_msg,
            }
            return HttpResponse(template.render(context, request))
        else:
            raise Http404("Recherche vide")
    else:
        raise Http404()

# Genere une liste de tweet personnalisé
def generateTimeline(user):
    # tweet de ses abonnements classe au plus recents, plus de like, retweet
    timeline = []

    for u in user.profil.abonnement.all():
        # twouitt des abonnements
        for msg in u.message_set.all():
            timeline.append({ "retwouitt": False, "msg": msg })

        # rt des abonnements
        for msg in user.profil.retweet_set.all():
            timeline.append({ "retwouitt": True, "msg": msg })

        # favoris des abonnements
        for msg in user.profil.favoris.all():
            timeline.append({ "retwouitt": False, "msg": msg })

    timeline = sorted(timeline, key=lambda item: item['msg'].created_at, reverse=True)

    return timeline