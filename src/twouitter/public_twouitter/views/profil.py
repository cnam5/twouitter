import datetime

from ..forms import AvatarForm, CoverForm

from django.http.response import Http404
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm

from django.contrib import messages




# # # # # # # # # # #
#                   #
#      Profil       #
#                   #
# # # # # # # # # # #

def profil(request, slug):
    template = loader.get_template("profil/profil.html")

    if slug == 'me':
        user = request.user
        is_me = True
    else:
        if slug == request.user.get_username():
            return redirect("profil",slug="me")
        is_me = False
        user = User.objects.get(username=slug)
        
        if not user:
            raise Http404("L'utilisateur n'existe pas")

    context = {
        'title': "Twouitter | Profil",
        'user' : user,
        'is_me': is_me,
    }
    return HttpResponse(template.render(context, request))


@login_required
def notifications(request):
    template = loader.get_template("profil/notifications.html")
    context = {
        'title': "Twouitter | Notifications",
    }
    return HttpResponse(template.render(context, request))
    

# # # # # # # # # # # #
#                     #
#      Settings       #
#                     #
# # # # # # # # # # # #

@login_required
def settings_general(request):
    template = loader.get_template("profil/settings-general.html")
    context = {
        'title'  : "Twouitter | Paramètres généraux",
        'commentaires' : request.user.profil.preferences.commentaires,
        'tags' : request.user.profil.preferences.tags,
        'friends_request' : request.user.profil.preferences.friends_request,
        'view_profil' : request.user.profil.preferences.view_profil,
        'send_request_friends' : request.user.profil.preferences.send_request_friends        
    }
    return HttpResponse(template.render(context, request))

@login_required
def settings_profil_info(request):
    template = loader.get_template("profil/settings-profil-info.html")
    user = request.user
    profil = user.profil

    context = {
        'title'  : "Twouitter | Informations sur le profil",
        'pseudo' : user.username,
        'first_name' : user.first_name,
        'last_name' : user.last_name,
        'description' : profil.about_me,
        'email' : user.email,
        'birthday' : profil.birthday.strftime("%Y-%m-%d"),
        'country' : profil.country,
        'city' : profil.city,
        'cover_form' : CoverForm(),
        'avatar_form' : AvatarForm()
    }

    return HttpResponse(template.render(context, request))

@login_required
def settings_account_info(request):
    template = loader.get_template("profil/settings-account-info.html")
    user = request.user

    context = {
        'title': "Twouitter | Informations du compte",
        'first_name' : user.first_name,
        'last_name' : user.last_name,
        'email' : user.email,
        'date_joined' : user.date_joined.strftime("%Y-%m-%d"),
    }
    return HttpResponse(template.render(context, request))

@login_required
def settings_password(request):
    template = loader.get_template("profil/settings-password.html")
    context = {
        'title': "Twouitter | Modifier mon mot de passe",
    }
    return HttpResponse(template.render(context, request))



# # # # # # # # # # # #
#                     #
#        Edit         #
#                     #
# # # # # # # # # # # #


@login_required
def settings_general_edit(request):
    if request.method=="POST":
        preferences = request.user.profil.preferences
        
        preferences.commentaires = request.POST['general_commentaires']
        preferences.tags = request.POST['general_tags']
        preferences.friends_request = request.POST['general_friends_request']
        preferences.view_profil = request.POST['general_view_profil']
        preferences.send_request_friends = request.POST['general_send_request_friends']

        preferences.save()

        messages.success(request, 'Vos préférences ont bien été modifiées.')

        return redirect("settings_general")

@login_required
def settings_profil_avatar_edit(request):
    if request.method == 'POST':
        form = AvatarForm(request.POST, request.FILES, instance = request.user.profil)
        if form.is_valid():
            form.save()
    return redirect("settings_profil_info")

@login_required
def settings_profil_cover_edit(request):
    if request.method == 'POST':
        form = CoverForm(request.POST, request.FILES, instance = request.user.profil)
        if form.is_valid():
            form.save()
    return redirect("settings_profil_info")


@login_required
def settings_profil_info_edit(request):
    if request.method=="POST":
        user = request.user
        profil = user.profil
        
        user.username = request.POST['profile_pseudo']
        user.email = request.POST['profile_email']

        profil.about_me = request.POST['profile_description']
        profil.country = request.POST['profile_country']
        profil.city = request.POST['profile_city']

        user.save()
        profil.save()
        
        messages.success(request, 'Vos informations ont bien été modifiées.')
        
        return redirect("settings_profil_info")

@login_required
def settings_account_info_edit(request):
    if request.method=="POST":
        user = request.user
        
        user.last_name = request.POST['account_last_name']
        user.first_name = request.POST['account_first_name']
        user.email = request.POST['account_email']

        user.save()

        messages.success(request, 'Vos informations personnelles ont bien été modifiées.')

        return redirect("settings_account_info")

@login_required
def settings_password_edit(request):
    if request.method=="POST":
        current_password = request.POST['account_current_password']
        new_password = request.POST['account_new_password']
        new_password_confirm = request.POST['account_new_password_confirm']

        user = authenticate(username = request.user.username, password = current_password)
        if user is not None:
            if new_password == new_password_confirm :
                request.user.set_password(new_password)
                request.user.save()
                update_session_auth_hash(request, request.user)
                messages.success(request, 'Votre mot de passe a bien été modifié.')
                return redirect("settings_password")
            else :
                messages.error(request, 'Les mots de passe ne sont pas identiques !', extra_tags='danger')
        else :
            messages.error(request, 'Votre mot de passe actuel est incorrect !', extra_tags='danger')
        
    return redirect("settings_password")
