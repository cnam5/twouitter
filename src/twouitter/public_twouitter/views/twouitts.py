from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.template import loader

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User

from ..forms import ImageForm
from ..models import Message
import uuid


# # # # # # # # # # #
#                   #
#      Twouitt      #
#                   #
# # # # # # # # # # #

@login_required
def add_twouitt(request):
    if request.method == "POST":
        POST = request.POST

        slug = uuid.uuid4().hex[:32]
        msg = Message.objects.create(user=request.user.profil, message=POST['quick-post-text'], slug=slug)
        form = ImageForm(request.POST, request.FILES, instance=msg)
        if form.is_valid():
            form.save()

        return redirect('twouitt', slug=slug)
    else:
        raise Http404("have to be POST")

def twouitt(request, slug):
    try:
        msg = Message.objects.get(slug__exact=slug)
    except ObjectDoesNotExist:
        raise Http404("Le twouitt n'existe pas")

    template = loader.get_template("twouitt/index.html")
    context = {
        'title': "Twouitter | Accueil",
        'msg': msg,
    }
    return HttpResponse(template.render(context, request))


@login_required
def retwouitt(request, slug):
    try:
        msg = Message.objects.get(slug__exact=slug)
    except ObjectDoesNotExist:
        raise Http404("Le twouitt n'existe pas")

    template = loader.get_template("twouitt/retwouitt.html")
    context = {
        'title': "Twouitter | Accueil",
        'rt_set': msg.retweet_set.filter(citation__isnull=False),
    }
    return HttpResponse(template.render(context, request))


@login_required
def add_favoris(request, slug):
    try:
        msg = Message.objects.get(slug__exact=slug)
    except ObjectDoesNotExist:
        raise Http404("Le twouitt n'existe pas")

    profil = request.user
    if msg.favoris.filter(user=profil):
        raise Http404("Le message est deja en favoris")
    else:
        msg.favoris.add(profil)


@login_required
def delete_favoris(request, slug):
    try:
        msg = Message.objects.get(slug__exact=slug)
    except ObjectDoesNotExist:
        raise Http404("Le twouitt n'existe pas")

    msg.favoris.get(request.user.profil)
    msg.favoris.remove(request.user.profil)


@login_required
def favoris_twouitt(request, slug):
    try:
        msg = Message.objects.get(slug__exact=slug)
    except ObjectDoesNotExist:
        raise Http404("Le twouitt n'existe pas")

    template = loader.get_template("twouitt/favoris.html")
    context = {
        'title': "Twouitter | Accueil",
        'favoris': msg.favoris,
    }
    return HttpResponse(template.render(context, request))
