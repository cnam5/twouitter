import json

from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import redirect

from django.db.models import Q
from django.core import serializers
from django.http import JsonResponse

from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST,require_safe

# # # # # # # # # # # # #
#                       #
#        Messages       #
#                       #
# # # # # # # # # # # # #
from ..models import MessagePrive, Profil


@login_required
def messages(request):
    template = loader.get_template("profil/messages.html")
    context = {
        'title': "Twouitter | Messages",
        'list_conversations' : get_conversations(request),
    }
    return HttpResponse(template.render(context, request))

# Retourne la liste des conversations
def get_conversations(request) :
    return request.user.profil.abonnes.all() | request.user.profil.abonnement.all()

# Retourne tous les messages d'une conversation donnée avec son id
@require_POST
def get_messages_conversation(request, slug) :
    return JsonResponse( list(MessagePrive.objects.filter( Q(expediteur_id=request.user.profil.id) & Q(destinataire_id=slug) |
                                                                      Q(expediteur_id=slug) & Q(destinataire_id=request.user.profil.id)  ).order_by('created_at').values()), safe=False)

# Envoi un message a un utilisateur
@require_POST
def send_message(request, slug) :
    message = request.POST['message']

    MessagePrive(
        expediteur_id=request.user.profil.id,
        destinataire_id=slug,
        message=message,
        is_read='False'
    ).save()

    return JsonResponse( list(MessagePrive.objects.filter( Q(expediteur_id=request.user.profil.id) & Q(destinataire_id=slug) |
                                                                      Q(expediteur_id=slug) & Q(destinataire_id=request.user.profil.id)  ).order_by('created_at').values()), safe=False)

# Permet de s'abonner à un compte
def abonnement(request, slug):
    request.user.profil.abonnes.add(Profil.objects.get(id=slug))
    Profil.objects.get(id=slug).abonnes.add(request.user.profil)


# Marquer tous les messages comme lus
def mark_all_messages_read(request) :
    MessagePrive.objects.filter(Q(destinataire_id=request.user.profil.id)).update(is_read='True')




