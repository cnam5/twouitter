from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.decorators import login_required

from ..models import Profil

# # # # # # # # # # # #  
#                     #
#        Friends      #
#                     #
# # # # # # # # # # # #

@login_required
def friends_list(request):
    template = loader.get_template("friends/friends-list.html")
    context = {
        'title': "Twouitter | Amis",
    }
    return HttpResponse(template.render(context, request))

@login_required
def friends_request(request):
    template = loader.get_template("friends/friends-request.html")
    context = {
        'title': "Twouitter | Demande d'ami",
        'demande_abonnement': get_request_abonnement(request)
    }
    return HttpResponse(template.render(context, request))

# Récupére demande d'abonnement
@login_required
def get_request_abonnement(request):
    return request.user.profil.abonnes.all()

# Permet de s'abonner à un compte
@login_required
def create_abonnement(request, slug):
    request.user.profil.abonnement.add(Profil.objects.get(id=slug))

@login_required
def delete_request(request, slug) :
    request.user.profil.abonnes.filter(from_profil_id=slug).delete() # FAIS ATTENTION PEUT SUPPRIMER LE PROFIL !!!