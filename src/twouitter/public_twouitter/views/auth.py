from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from ..models import Profil, Preferences

from django.core.mail import send_mail

# # # # # # # # # # #
#                   #
#  Authentification #
#                   #
# # # # # # # # # # #


# Affiche la page de connexion et d'inscription
def secure(request):
    template = loader.get_template("secure/index.html")
    context = {
        'title': "Twouitter | Login",
    }
    return HttpResponse(template.render(context, request))

# Permet de connecter un utilisateur
def login(request):
    if request.method=="POST":
        username = request.POST['login_username']
        password = request.POST['login_password']
        remember_me = request.POST.get('login_remember', 0)

        user = authenticate(username = username, password = password)
        if user is not None:
            auth_login(request, user)
            if remember_me == 0:
                    request.session.set_expiry(0)
            return HttpResponseRedirect("/home")
        else:
            return HttpResponseRedirect("/")
    else:
        return HttpResponseRedirect("/")

# Permet d'inscrire un nouvel utilisateur
def register(request):
    if request.method=="POST":
        email = request.POST['register_email']
        username = request.POST['register_username']
        gender = request.POST['register_gender']
        lastname = request.POST['register_lastname']
        firstname = request.POST['register_firstname']
        birthday = request.POST['register_birthday']
        password = request.POST['register_password']

        user = User.objects.create_user(
            email=email, 
            username=username, 
            last_name=lastname, 
            first_name=firstname,
            password=password)

        user.save()

        profil = Profil.objects.create(
            user_id=user.id,
            gender=gender,
            birthday=birthday
        ).save()

        Preferences.objects.create(
            profil_id=user.profil.id,
            commentaires=1,
            tags=1,
            friends_request=1
        ).save()

        return HttpResponseRedirect("/")

# Affiche la page de mot de passe oublié
def lostpassword(request):
    template = loader.get_template("secure/lost_password.html")
    context = {
        'title': "Twouitter | Mot de passe Oublié",
    }
    return HttpResponse(template.render(context, request))

# Permet de renvoyer un nouveau mot de passe à un utilisateur
def forgetpassword(request):
    email = request.POST['forget_email']

    # send_mail(
    #     'Réinitialisation de votre mot de passe Twouitter',
    #     'Veuillez trouver ci-dessous votre nouveau mot de passe Twouitter : .... . Nous vous conseillons de le modifier dès votre prochaine connexion',
    #     'from@example.com',
    #     ['to@example.com'],
    #     fail_silently=False,
    # )

    return HttpResponseRedirect("/")

# Permet de déconnecter un utilisateur  
@login_required
def logout(request):
    auth_logout(request)
    return HttpResponseRedirect("/")

# Permet de supprimer un utilisateur  
def deleteuser(request):
    u = User.objects.get(username = "az")
    u.delete()