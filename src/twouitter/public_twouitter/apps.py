from django.apps import AppConfig


class PublicTwouitterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'public_twouitter'
