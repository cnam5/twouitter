CREATE TABLE UTILISATEUR(
   id_utilisateur INT,
   pseudo VARCHAR(100),
   email VARCHAR(200),
   password VARCHAR(255),
   lastname VARCHAR(100),
   firstname VARCHAR(100),
   gender CHAR(5),
   birthday_date DATE,
   city VARCHAR(100),
   country VARCHAR(100),
   aboutme VARCHAR(248),
   PRIMARY KEY(id_utilisateur)
);

CREATE TABLE NOTIFICATION(
   id_notification INT,
   message VARCHAR(255),
   id_utilisateur INT NOT NULL,
   PRIMARY KEY(id_notification),
   FOREIGN KEY(id_utilisateur) REFERENCES UTILISATEUR(id_utilisateur)
);

CREATE TABLE MEDIA(
   id_media INT,
   type VARCHAR(50),
   name VARCHAR(255),
   PRIMARY KEY(id_media)
);

CREATE TABLE MESSAGE(
   id_message INT,
   message VARCHAR(280),
   slug CHAR(20),
   id_utilisateur INT NOT NULL,
   id_media INT,
   id_message_1 INT,
   PRIMARY KEY(id_message),
   UNIQUE(id_media),
   UNIQUE(slug),
   FOREIGN KEY(id_utilisateur) REFERENCES UTILISATEUR(id_utilisateur),
   FOREIGN KEY(id_media) REFERENCES MEDIA(id_media),
   FOREIGN KEY(id_message_1) REFERENCES MESSAGE(id_message)
);

CREATE TABLE MESSAGE_PV(
   id_utilisateur INT,
   id_utilisateur_1 INT,
   id_message_pv INT,
   message VARCHAR(280),
   PRIMARY KEY(id_utilisateur, id_utilisateur_1),
   FOREIGN KEY(id_utilisateur) REFERENCES UTILISATEUR(id_utilisateur),
   FOREIGN KEY(id_utilisateur_1) REFERENCES UTILISATEUR(id_utilisateur)
);

CREATE TABLE FOLLOW(
   id_utilisateur INT,
   id_utilisateur_1 INT,
   PRIMARY KEY(id_utilisateur, id_utilisateur_1),
   FOREIGN KEY(id_utilisateur) REFERENCES UTILISATEUR(id_utilisateur),
   FOREIGN KEY(id_utilisateur_1) REFERENCES UTILISATEUR(id_utilisateur)
);

CREATE TABLE PHOTO_PROFIL(
   id_utilisateur INT,
   id_media INT NOT NULL,
   PRIMARY KEY(id_utilisateur),
   UNIQUE(id_media),
   FOREIGN KEY(id_utilisateur) REFERENCES UTILISATEUR(id_utilisateur),
   FOREIGN KEY(id_media) REFERENCES MEDIA(id_media)
);

CREATE TABLE BANNIERE(
   id_utilisateur INT,
   id_media INT NOT NULL,
   PRIMARY KEY(id_utilisateur),
   UNIQUE(id_media),
   FOREIGN KEY(id_utilisateur) REFERENCES UTILISATEUR(id_utilisateur),
   FOREIGN KEY(id_media) REFERENCES MEDIA(id_media)
);

CREATE TABLE FAVORIS(
   id_utilisateur INT,
   id_message INT,
   PRIMARY KEY(id_utilisateur, id_message),
   FOREIGN KEY(id_utilisateur) REFERENCES UTILISATEUR(id_utilisateur),
   FOREIGN KEY(id_message) REFERENCES MESSAGE(id_message)
);

CREATE TABLE RETWEET(
   id_utilisateur INT,
   id_message INT,
   PRIMARY KEY(id_utilisateur, id_message),
   FOREIGN KEY(id_utilisateur) REFERENCES UTILISATEUR(id_utilisateur),
   FOREIGN KEY(id_message) REFERENCES MESSAGE(id_message)
);
