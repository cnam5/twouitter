# Tables et Relations

## Tables à modifier

### User

Ajouter les champs suivants dans la table: 

- lastname : varchar
- firstname : varchar
- gender : varchar
- birthday : date
- city : varchar
- country : varchar
- aboutme : varchar
- suspended : tinyint
- id_photo : id
- id_banniere : id

**Attributs Optionnel** : aboutme, country, city, id_photo, id_banniere

## Tables à créer

### Media

```bash
php oil g model media id:int type:varchar[50] name:varchar[255] deleted_at:int
```

### Notification

```bash
php oil g model Notification id:int id_user:int message:varchar[255] is_read:tinyint deleted_at:int
```

### Message

```bash
php oil g model Message id:int id_user:int id_message_parent:int id_photo:int message:varchar[280] slug:char[20] deleted_at:int 
```

**Attributs Optionnel** : id_message_parent, id_photo

### MessagePv

```bash
php oil g model MessagePv id:int id_user:int id_user_destinataire:int message:varchar[280] is_read:tinyint deleted_at:int 
```

### Follow

```bash
php oil g model Follow id:int id_user:int id_user_follow:int deleted_at:int
```

### Retweet

```bash
php oil g model Retweet id:int id_user:int id_message:int citation:varchar[280] deleted_at:int
```

**Attributs Optionnel** : citation

### Favori

```bash
php oil g model Favori id:int id_user:int id_message:int deleted_at:int
```

### SignalementUtilisateur

```bash
php oil g model SignalementUtilisateur id:int id_user:int id_user_signale:int motif:varchar[255] deleted_at:int
```

### SignalementMessage

```bash
php oil g model SignalementMessage id:int id_user:int id_message:int motif:varchar[255] deleted_at:int
```

### UserBloque

```bash
php oil g model UserBloque id:int id_userbloque:int id_userbloque_par:int deleted_at:int
```

## Relations

### Belongs To

#### Notifie

Ajout de **id_utilisateur** dans la table **notification**

### Has one

#### Tweet

Ajout de **id_user** dans la table **message**

#### Inclus

Ajout de **id_photo** dans la table **message**

#### Commente

Ajout de **id_message_parent** dans la table **message**

#### Photo_profil

Ajout de **id_photo** dans la table **user**

#### Banniere

Ajout de **id_banniere** dans la table **user**

#### Message privé

Ajout de **id_user** dans la table **messagepv**
Ajout de **id_user_destinataire** dans la table **messagepv**

#### Follow

Ajout de **id_user** dans la table **follow**
Ajout de **id_user_followed** dans la table **follow**

#### Signalement utilisateur

Ajout de **id_user** dans la table **signalement_utilisateur**
Ajout de **id_user_signale** dans la table **signalement_utilisateur**

#### Signalement message

Ajout de **id_user** dans la table **Signalement_message**
Ajout de **id_message** dans la table **Signalement_message**

#### Retweet

Ajout de **id_user** dans la table **retweet**
Ajout de **id_message** dans la table **retweet**

#### Favoris

Ajout de **id_user** dans la table **favoris**
Ajout de **id_message** dans la table **favoris**

# Créer les tables dans la base de données

```bash
# Génére les modèles crées dans la base de donnée
php oil r migrate

# Génére les tables du packages auth dans la base de donnée
php oil r migrate --packages=auth
```
